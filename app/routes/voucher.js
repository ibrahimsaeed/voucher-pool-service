const express = require('express')
const router = express.Router()
const voucherController = require('../controllers/voucher')

// Get Request to retrieve voucher using code.
router.get('/:code/status', voucherController.getVoucher)

// POST Request to create voucher.
router.post('/create', voucherController.createVoucher)
// POST Request to create bulk vouchers.
router.post('/bulk-create', voucherController.createBulkVouchers)

module.exports = router
