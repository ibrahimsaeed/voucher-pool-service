const _ = require('lodash')
const request = require('supertest')
const app = require('../../app')
const Voucher = require('../models/voucher')

describe('Get Voucher Status', () => {
  it('should get specific voucher using code param', async () => {
    let code = `Voucher-${_.random(11111, 99999)}`

    // Create Voucher
    Voucher.create({
      code,
      amount: 50,
      type: 'percentage',
      expiry_date: '2024-01-01T00:00:00.000Z'
    })

    // Get Voucher Status
    const res = await request(app).get(`/vouchers/${code}/status`)
    const responseBody = res.body[0]

    // Check Response result
    expect(res.statusCode).toEqual(200)
    expect(responseBody.code).toBeDefined()
    expect(responseBody.amount).toBeDefined()
    expect(responseBody.type).toBeDefined()
    expect(responseBody.expiry_date).toBeDefined()
  })
})

describe('Create Single Voucher', () => {
  it('should create single voucher', async () => {
    let code = `Voucher-${_.random(11111, 99999)}`

    // Create New Voucher
    const res = await request(app).post('/vouchers/create').send({
      code: code,
      amount: 10,
      type: 'percentage',
      expiry_date: '2024-01-01T00:00:00.000Z'
    })

    const responseBody = res.body

    // Check Response result
    expect(res.statusCode).toEqual(201)
    expect(responseBody.newVoucher.code).toEqual(code)
    expect(responseBody.newVoucher.amount).toEqual(10)
    expect(responseBody.newVoucher.type).toEqual('percentage')
    expect(responseBody.newVoucher.expiry_date).toEqual(
      '2024-01-01T00:00:00.000Z'
    )
  })
})

describe('Create Bulk Vouchers', () => {
  it('should create Bulk of vouchers', async () => {
    // Create New Voucher
    const res = await request(app).post('/vouchers/bulk-create').send({
      amount: 25,
      type: 'fixed',
      expiry_date: '2024-01-01T00:00:00.000Z'
    })

    const responseBody = res.body

    // Check Response result
    expect(res.statusCode).toEqual(200)
    expect(responseBody.message).toEqual('success')
  })
})
