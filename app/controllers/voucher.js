const _ = require('lodash')
const Voucher = require('../models/voucher')
const User = require('../models/user')

// Get voucher status
exports.getVoucher = (req, res) => {
  Voucher.find({ code: req.params.code })
    .exec()
    .then((vouchers) => {
      res.status(200).json(vouchers)
    })
    .catch((err) => {
      res.status(500).json({
        message: err.message
      })
    })
}

// Create single Voucher
exports.createVoucher = (req, res) => {
  const newVoucher = new Voucher({
    code: req.body.code,
    amount: req.body.amount,
    type: req.body.type,
    expiry_date: req.body.expiry_date
  })
  Voucher.create(newVoucher)
    .then((newVoucher) => {
      res.status(201).json({ newVoucher })
    })
    .catch((err) => {
      res.status(500).json({
        message: err.message
      })
    })
}

// Create Bulk Voucher
exports.createBulkVouchers = async (req, res) => {
  let cursor = User.find().cursor()
  cursor.eachAsync((user) => {
    Voucher.create({
      code: _.random(11111, 99999),
      amount: req.body.amount,
      type: req.body.type,
      expiry_date: req.body.expiry_date,
      used_by: user._id
    }).catch((err) => {
      res.status(500).json({ message: err.message })
    })
  })
  res.status(200).json({ message: 'success' })
}
