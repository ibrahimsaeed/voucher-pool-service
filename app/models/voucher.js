const mongoose = require('mongoose')

const voucherSchema = mongoose.Schema(
  {
    code: {
      type: String,
      required: true,
      unique: true
    },
    amount: {
      type: Number
    },
    type: {
      type: String,
      enum: ['fixed', 'percentage']
    },
    expiry_date: {
      type: Date
    },
    is_used: {
      type: Boolean,
      default: false
    },
    used_by: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User'
    },
    usage_date: {
      type: Date
    }
  },
  { timestamps: true }
)

module.exports = mongoose.model('Voucher', voucherSchema)
