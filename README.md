# Voucher-Pool-Service

Simple Service To generate vouchers .

## Installation

clone the repository
`git clone https://gitlab.com/ibrahimsaeed/voucher-pool-service`

Switch to repository folder
`cd voucher-pool-service`

## Run Tests

run `npm run test`

## Run Service

run `npm run dev`

## Available APIs

- `GET    /vouchers/:code/status`
- `POST   /vouchers/create`
- `POST   /vouchers/bulk-create`

Create single Voucher Body

```json
{
  "code": "voucher-2023",
  "amount": "50",
  "type": "percentage",
  "expiry_date": "01-01-2024"
}
```

Create Bulk Voucher Body

```json
{
  "amount": "10",
  "type": "fixed",
  "expiry_date": "01-01-2024"
}
```

## 

Let`s Talk : [Linkedin](https://www.linkedin.com/in/developer-ibrahim-saeed/)
